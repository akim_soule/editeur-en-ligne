var scene;
var camera;
var renderer;
var objects = [];
var div = '#dessin';
var startX, startY;
var startColor;
var light ;
var objectCourant;
var objetAColler;
var objectSelectionnes = [];
var cntrlIsPressed = false;
var thereIsSelection = false;
var currentListSelected;
// var objectACollers = [];

initierSceneEtAjouterAuDiv(div);
animate();

function ajouter(argument) {
  let object = FabriqueElement(argument);
  setPosition(object, 0, 0);
  addToScene(object);
}

function removeFromScene(element) {
  let object = scene.getObjectById(element.id,true );
  if (object !== undefined) {
    scene.remove(object);
    for( var i = 0; i < objects.length; i++){ 
     if ( objects[i].id !== undefined && objects[i].id === element.id) {
       objects.splice(i, 1); 
     }
   }
   element.material.dispose();
   element.geometry.dispose();
   new Commande(ACTION.SUPPRIMER, object);
 }
}

function addToScene(element) {
  scene.add(element);
  objects.push(element);
  new Commande(ACTION.AJOUTER, element);
  render();
}

function setPosition(element, x, y) {
  element.position.set(x, y, 0);
}

function initierSceneEtAjouterAuDiv(elem){
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(75, $(elem).width()/
    $(elem).height(), 0.1, 1000 );
  camera.position.z = 30;
  scene.background = new THREE.Color("rgb(158, 158, 158)");
  renderer = new THREE.WebGLRenderer();
  renderer.setSize($(elem).width(), $(elem).height() );
  document.getElementById('dessin').appendChild( renderer.domElement );
  camera.position.z = 50;
  var ambientLight = new THREE.AmbientLight( 0x0f0f0f );
  scene.add(ambientLight);
  
  var light = new THREE.SpotLight(0xffffff, 1.5);
  light.position.set(0, 500, 2000);
  scene.add(light);

  actualiserControls(objects);

  window.addEventListener( 'resize', onWindowResize, false );
  window.addEventListener( 'click', onclick, false );
}

function onWindowResize() {
  camera.aspect = $(div).width()/$(div).height();
  camera.updateProjectionMatrix();
  renderer.setSize($(div).width(), $(div).height());
}

function animate() {
  requestAnimationFrame( animate );
  render();
}

function render() {
  renderer.render( scene, camera );
}

function actualiserControls(argument) {
  var controls = new THREE.DragControls( argument, camera, renderer.domElement );
  controls.addEventListener('mousedown', function (event) {
    
  });
  controls.addEventListener( 'dragstart', function ( event ) {
    event.object.material.emissive.set( 0xaaaaaa );
    objectCourant = event.object;
    console.log(objectCourant)
    if (cntrlIsPressed){
      console.log('le control');
      // if (!objectSelectionnes.includes(objectCourant)) {
        objectSelectionnes.push(objectCourant);
      // }
      

    }

  });

  controls.addEventListener( 'dragend', function ( event ) {
    event.object.material.emissive.set( 0x000000 );
    objectCourant = event.object;
  });
}


  function selectionnerTout() {
    objectSelectionnes = objects;
  }

  function onclick(){
    if (!cntrlIsPressed){
      objectSelectionnes = [];
    }      
  }

