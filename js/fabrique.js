    function FabriqueElement(indice) {
          var material = new THREE.MeshLambertMaterial({ color: Math.random() * 0xffffff, transparent : true });

          switch(indice){
            case 0 :
            //caree
            geometry = new THREE.BoxGeometry(5, 5, 0.05 );
            element = new THREE.Mesh(geometry, material);
            break;
            case 1:
            //rectangle
            geometry = new THREE.BoxGeometry(10, 5, 0.05);
            element = new THREE.Mesh(geometry, material);
            break;
            case 2:
            //cercle
            geometry = new THREE.CircleGeometry( 5, 32 );
            element = new THREE.Mesh(geometry, material);
            break;
            case 3:
            //triangle
            geometry = new THREE.CircleGeometry( 5, 3);
            element = new THREE.Mesh(geometry, material);
            break;
            case 4:
            //losange
            geometry = new THREE.CircleGeometry( 5, 4);
            element = new THREE.Mesh(geometry, material);
            break;
            case 5 :
            //pentagone
            geometry = new THREE.CircleGeometry( 5, 5);
            element = new THREE.Mesh(geometry, material);
            break;
            case 6 :
            //hexagone
            geometry = new THREE.CircleGeometry( 5, 6);
            element = new THREE.Mesh(geometry, material);
            break;
            case 7 :
            //croix
            var rect1Croix = new THREE.Mesh(new THREE.BoxGeometry(10, 2, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));
            var rect2 = new THREE.Mesh(new THREE.BoxGeometry(10, 2, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));
            var rectCroixMesh = new THREE.Mesh(rect1Croix.geometry);
            var rect2Mesh = new THREE.Mesh(rect2.geometry); 
            rect2Mesh.geometry.parameters.width = 5;
            rect2Mesh.rotation.z = Math.PI / 2;
            rectCroixMesh.updateMatrix();
            rect2Mesh.updateMatrix();
            var singleCroixGeometry = new THREE.Geometry();
            singleCroixGeometry.merge(rectCroixMesh.geometry, rectCroixMesh.matrix);
            singleCroixGeometry.merge(rect2Mesh.geometry, rect2Mesh.matrix);
            var element = new THREE.Mesh(singleCroixGeometry, 
                  new THREE.MeshPhongMaterial({color: Math.random() * 0xffffff, transparent : true }));
            break;
            case 8:
            //fleche
            var rect3 = new THREE.Mesh(new THREE.BoxGeometry(10, 3, 0.05 ),
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));
            var triang = FabriqueElement(3);
            var rect3Mech = new THREE.Mesh(rect3.geometry);
            var triangMesh = new THREE.Mesh(triang.geometry); 
            triangMesh.position.x = 6;
            rect3Mech.updateMatrix();
            triangMesh.updateMatrix();
            var singleFlecheGeometry = new THREE.Geometry();
            singleFlecheGeometry.merge(rect3Mech.geometry, rect3Mech.matrix);
            singleFlecheGeometry.merge(triangMesh.geometry, triangMesh.matrix);
            var element = new THREE.Mesh(singleFlecheGeometry, new THREE.MeshPhongMaterial({ color: Math.random() * 0xffffff, transparent : true }));
            
            break;
            case 9:
            //voiture
            let rect1Voiture = new THREE.Mesh(new THREE.BoxGeometry(15, 5, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));

            let rect2Voiture = new THREE.Mesh(new THREE.BoxGeometry(7, 5, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));

            let rect3Voiture = new THREE.Mesh(new THREE.BoxGeometry(7, 5, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));

            let rect4Voiture = new THREE.Mesh(new THREE.BoxGeometry(35, 6, 0.05 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));

            let pneuG = new THREE.Mesh(new THREE.CircleGeometry(3, 32 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));
            
            let pneuD = new THREE.Mesh(new THREE.CircleGeometry(3, 32 ), 
                  new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, transparent : true }));

            let rectMech = new THREE.Mesh(rect1Voiture.geometry);
            let rect2VoituregMesh = new THREE.Mesh(rect2Voiture.geometry); 
            let rect3VoituregMesh = new THREE.Mesh(rect3Voiture.geometry); 
            let rect4VoituregMesh = new THREE.Mesh(rect4Voiture.geometry); 
            let pneuGgMesh = new THREE.Mesh(pneuG.geometry); 
            let pneuDgMesh = new THREE.Mesh(pneuD.geometry); 

            rect2VoituregMesh.rotation.z = 7;
            rect2VoituregMesh.position.x = -8.5;
            rect2VoituregMesh.position.y = -1.5;

            rect3VoituregMesh.rotation.z = -7;
            rect3VoituregMesh.position.x = 8.5;
            rect3VoituregMesh.position.y = -1.5;

            rect4VoituregMesh.position.y = -5;

            pneuGgMesh.position.x = -8.5;
            pneuGgMesh.position.y = -8;

            pneuDgMesh.position.x = 8.5;
            pneuDgMesh.position.y = -8;


            rectMech.updateMatrix();
            rect2VoituregMesh.updateMatrix();
            rect3VoituregMesh.updateMatrix();
            rect4VoituregMesh.updateMatrix();
            pneuGgMesh.updateMatrix();
            pneuDgMesh.updateMatrix();

            let singleVoitureGeometry = new THREE.Geometry();
            singleVoitureGeometry.merge(rectMech.geometry, rectMech.matrix);
            singleVoitureGeometry.merge(rect2VoituregMesh.geometry, rect2VoituregMesh.matrix);
            singleVoitureGeometry.merge(rect3VoituregMesh.geometry, rect3VoituregMesh.matrix);
            singleVoitureGeometry.merge(rect4VoituregMesh.geometry, rect4VoituregMesh.matrix);
            singleVoitureGeometry.merge(pneuGgMesh.geometry, pneuGgMesh.matrix);
            singleVoitureGeometry.merge(pneuDgMesh.geometry, pneuDgMesh.matrix);

            var element = new THREE.Mesh(singleVoitureGeometry, material);
            break;
            case 10:
            //bonhomme
            var tete = new THREE.CircleGeometry( 5, 32 ); 
            var gorge= new THREE.Shape();
            gorge.moveTo( 0, 0 );
            gorge.lineTo( 0, 5 );
            gorge.lineTo( 2.5, 5 );
            gorge.lineTo( 2.5, 0 );
            var body  = new THREE.Shape();
            body.moveTo( 0, 0 );
            body.lineTo( 0, 20 );
            body.lineTo( 12, 20 );
            body.lineTo( 12, 0 );   
            var leftArm = new THREE.Shape();
            leftArm.moveTo( 0.0, 0.0 );
            leftArm.lineTo(  4.0, 17.0);
            leftArm.lineTo( 4.0, 12.0);
            leftArm.lineTo( 2, 0.0 );

            var rightArm = new THREE.Shape();
            rightArm.moveTo( 0.0, 0.0 );
            rightArm.lineTo(  -4.0, 17.0);
            rightArm.lineTo( -4.0, 12.0);
            rightArm.lineTo( -2, 0.0 );

            var leftleg = new THREE.Shape();
            leftleg.moveTo( 0.0, 0.0 );
            leftleg.lineTo(  0.0, 16.0);
            leftleg.lineTo( 12.0, 16.0);
            leftleg.lineTo( 12.0, 0.0 );
            leftleg.lineTo( 8.0, 0.0 );
            leftleg.lineTo( 6.0, 12.0 );
            leftleg.lineTo( 4.0, 0.0 );


            var singleGeometry = new THREE.Geometry();

            var sphereMesh = new THREE.Mesh(tete);
            var yy =  new THREE.ShapeGeometry( gorge );
            var bud =  new THREE.ShapeGeometry( body );
            var rr =  new THREE.ShapeGeometry( rightArm );
            var xx =  new THREE.ShapeGeometry( leftArm );
            var leg1 =  new THREE.ShapeGeometry( leftleg );   
            var boxMesh = new THREE.Mesh(yy);
            var boxMesh2 = new THREE.Mesh(bud);
            var boxMesh3 = new THREE.Mesh(xx);
            var boxMesh4 = new THREE.Mesh(rr);
            var boxMesh5 = new THREE.Mesh(leg1);  


            boxMesh.position.x = -1;
            boxMesh.position.y = -3;
            boxMesh.position.z = 0;

            boxMesh2.position.x = -6;
            boxMesh2.position.z = 0;
            boxMesh2.position.y = -22;


            boxMesh3.position.x = -10;
            boxMesh3.position.y = -19;
            boxMesh3.position.z = 0;

            boxMesh4.position.x = 10;
            boxMesh4.position.y = -19;
            boxMesh4.position.z = 0;

            boxMesh5.position.x = -6;
            boxMesh5.position.y = -38;
            boxMesh5.position.z = 0;


            sphereMesh.position.y = 4;
            sphereMesh.position.z = 0;


            boxMesh.updateMatrix(); 
            singleGeometry.merge(boxMesh.geometry, boxMesh.matrix);

            boxMesh2.updateMatrix(); 
            singleGeometry.merge(boxMesh2.geometry, boxMesh2.matrix);

            sphereMesh.updateMatrix(); 
            singleGeometry.merge(sphereMesh.geometry, sphereMesh.matrix);

            boxMesh3.updateMatrix(); 
            singleGeometry.merge(boxMesh3.geometry, boxMesh3.matrix);

            boxMesh4.updateMatrix(); 
            singleGeometry.merge(boxMesh4.geometry, boxMesh4.matrix);

            boxMesh5.updateMatrix(); 
            singleGeometry.merge(boxMesh5.geometry, boxMesh5.matrix);

            var element = new THREE.Mesh(singleGeometry, material);
            element.scale.set(0.4,0.4,0.4);
            break;
            case 11 :
            //coeur
            var x = 0;
            var y = 0;
            var heartShape = new THREE.Shape();
            heartShape.moveTo( x + 2.5, y + 2.5 );
            heartShape.bezierCurveTo( x + 2.5, y + 2.5, x + 2, y, x, y );
            heartShape.bezierCurveTo( x - 3, y, x - 3, y + 3.5,x - 3,y + 3.5 );
            heartShape.bezierCurveTo( x - 3, y + 5.5, x - 1, y + 7.7, x + 2.5, y + 9.5 );
            heartShape.bezierCurveTo( x + 6, y + 7.7, x + 8, y + 5.5, x + 8, y + 3.5 );
            heartShape.bezierCurveTo( x + 8, y + 3.5, x + 8, y, x + 5, y );
            heartShape.bezierCurveTo( x + 3.5, y, x + 2.5, y + 2.5, x + 2.5, y + 2.5 );
            var geometry = new THREE.ShapeGeometry( heartShape );

            var element = new THREE.Mesh(geometry, material);
            element.rotation.z=-3.2 ;
            element.scale.set(0.5,0.5,0.5);

            break;
            case 12 :
            //avion
            var singleGeometry = new THREE.Geometry();
            var body  = new THREE.Shape();
            body.moveTo( 0, 0 );
            body.lineTo( 0, 20 );
            body.lineTo( 8, 20 );
            body.lineTo( 8, 0 ); 
            var bud =  new THREE.ShapeGeometry( body );
            var bodyMesh = new THREE.Mesh(bud);
            bodyMesh.updateMatrix(); 
            singleGeometry.merge(bodyMesh.geometry, bodyMesh.matrix);
            var left  = new THREE.Shape();
            left.moveTo( 0, 0 );
            left.lineTo( 10, 8 );
            left.lineTo( 10, 0 ); 
            var leftwing =  new THREE.ShapeGeometry( left );
            var leftWingMesh = new THREE.Mesh(leftwing);
            leftWingMesh.position.x = -10;
            leftWingMesh.position.y = 8;
            leftWingMesh.position.z = 0;
            leftWingMesh.updateMatrix(); 
            singleGeometry.merge(leftWingMesh.geometry, leftWingMesh.matrix);
            var right  = new THREE.Shape();
            right.moveTo( 0, 0 );
            right.lineTo( -10, 8 );
            right.lineTo( -10, 0 ); 
            var rigthwing =  new THREE.ShapeGeometry( right );
            var rightWingMesh = new THREE.Mesh(rigthwing);
            rightWingMesh.position.x = 18;
            rightWingMesh.position.y = 8;
            rightWingMesh.position.z = 0;
            rightWingMesh.updateMatrix(); 
            singleGeometry.merge(rightWingMesh.geometry, rightWingMesh.matrix);
            var tete  = new THREE.Shape();
            tete.moveTo( 0, 0 );
            tete.lineTo( 3, 4 );
            tete.lineTo( 6, 0 ); 
            var headPlane =  new THREE.ShapeGeometry( tete );
            var headPlaneMesh = new THREE.Mesh(headPlane);
            headPlaneMesh.position.x = 1;
            headPlaneMesh.position.y = 20;
            headPlaneMesh.position.z = 0;
            headPlaneMesh.updateMatrix(); 
            singleGeometry.merge(headPlaneMesh.geometry, headPlaneMesh.matrix);
            var queu  = new THREE.Shape();
            queu.moveTo( 3.5, 0 );
            queu.lineTo( 0, 4 );
            queu.lineTo( -3.5, 0 ); 
            var TailPlane =  new THREE.ShapeGeometry( queu );
            var TailPlaneMesh = new THREE.Mesh(TailPlane);
            TailPlaneMesh.position.x = 4;
            TailPlaneMesh.position.y = -2.5;
            TailPlaneMesh.position.z = 0;
            TailPlaneMesh.updateMatrix(); 
            singleGeometry.merge(TailPlaneMesh.geometry, TailPlaneMesh.matrix);
            var element = new THREE.Mesh(singleGeometry, material);
            element.scale.set(0.5,0.5,0.5);
            break;
            case 13:
            
            var pyramidgeometry = new THREE.CylinderGeometry(5, 10, 16, 4, false);
            var element = new THREE.Mesh(pyramidgeometry, material);

            break;
            default:
            //caree
            geometry = new THREE.BoxGeometry(5, 5, 0.05 );
            element = new THREE.Mesh(geometry, material);
            break;

      }

      return element;
}