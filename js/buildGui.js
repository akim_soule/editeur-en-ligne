
function buildGui() {

    var gui = new dat.GUI( { width: 300 } );
    
    var param = {

        width: 10,
        height: 10,
        color: 0xffffff,
        // intensity: light.intensity,
        transparency: 0.90,
        rotationX : 10,
        rotationY : 10,
        rotationZ : 10,
        positionZ : 10,
        // prismeFace1_longueur : 10,
        influence1: 0,
        influence2: 0,
        influence3: 0,
        influence4: 0,
        influence5: 0,
        influence6: 0,
        influence7: 0,
        influence8: 0
        // prismeFace1_largeur : 10,
        // prismeFace2_longueur : 10,
        // prismeFace2_largeur : 10,




    };

    var obj = { duplicate:function(){ 

        if (objectCourant != null){

            var clone = objectCourant.clone();
            
            if (objectCourant.material.map != null) {
                console.log(objectCourant.material.map.image.attributes[0]);
                var loader = new THREE.TextureLoader();
                var img = new String(objectCourant.material.map.image.attributes[0]);
                var mat = new THREE.MeshLambertMaterial({
                    map: loader.load(img),
                    transparent : true
                    // map: loader.load('https://s3.amazonaws.com/duhaime/blog/tsne-webgl/assets/cat.jpg')
                });
            }else{
                var mat = new THREE.MeshLambertMaterial( { color: objectCourant.material.color, transparent : true } );
                clone.material = mat;
            }
            
            
            for (var i = 0; i < objects.length; i++) {
              if (objects[i].position.x == clone.position.x || objects[i].position.y == clone.position.y) {
                setPosition(clone, objects[i].position.x +2, 
                  objects[i].position.y - 2);
            }
        }
        addToScene(clone);


    }
    
},
delete:function(){
    if (objects.length > 0 && objectCourant != undefined){
        removeFromScene(objectCourant);
    }
},
fusion:function() {
    var objetMesh = [];
    for (var i = 0; i < objectSelectionnes.length; i++) {

        var meshObji = new THREE.Mesh(objectSelectionnes[i].geometry);
        
        meshObji.position.x = objectSelectionnes[i].position.x;
        meshObji.position.y = objectSelectionnes[i].position.y;
        meshObji.position.z = objectSelectionnes[i].position.z;
        meshObji.scale.x = objectSelectionnes[i].scale.x;
        meshObji.scale.y = objectSelectionnes[i].scale.y;
        meshObji.scale.z = objectSelectionnes[i].scale.z;
        meshObji.rotation.x = objectSelectionnes[i].rotation.x;
        meshObji.rotation.y = objectSelectionnes[i].rotation.y;
        meshObji.rotation.z = objectSelectionnes[i].rotation.z;

        meshObji.updateMatrix();
        objetMesh.push(meshObji);
    }

    var fusion = new THREE.Geometry();
    while(objetMesh.length > 0){
       let of =  objetMesh.pop();
       of.position.x = 
       fusion.merge(of.geometry, of.matrix);
   }
   var elementFusion = new THREE.Mesh(fusion, 
    new THREE.MeshPhongMaterial({ color: Math.random() * 0xffffff, transparent : true }));
   addToScene(elementFusion);
},
zoomAvant:function () {
    camera.position.z = camera.position.z - 1;
},
zoomArriere:function () {
    camera.position.z = camera.position.z + 1;
}

};


var lightFolder = gui.addFolder( 'Outils' );
lightFolder.add( param, 'width', 0.001, 20 ).step( 0.1 ).onChange( function ( val ) {
    if (objectCourant != null){
        objectCourant.width = val;
        objectCourant.scale.x = val;
    }

} );
lightFolder.add( param, 'height', 0.001, 20 ).step( 0.1 ).onChange( function ( val ) {
    if (objectCourant != null){
      objectCourant.height = val;
      objectCourant.scale.y = val;
  }

} );

lightFolder.addColor( param, 'color' ).onChange( function () {
    if (objectCourant != null){
        objectCourant.material.color.set( param.color );
    }


});

lightFolder.add( param, 'transparency', 0, 1 )
.onChange( function () {
    if (objectCourant != null){
       objectCourant.material.opacity  = param.transparency;

   }

} );
lightFolder.add( param, 'rotationX', 0, 20 ).step( 0.01 )
.onChange( function () {
    if (objectCourant != null){
        objectCourant.rotation.x = param.rotationX;

    }

} );
lightFolder.add( param, 'rotationY', 0, 20 )
.onChange( function () {
    if (objectCourant != null){
        objectCourant.rotation.y = param.rotationY;

    }

} );
lightFolder.add( param, 'rotationZ', 0, 20 )
.onChange( function () {
    if (objectCourant != null){
        objectCourant.rotation.z = param.rotationZ;
    }

});
lightFolder.add( param, 'positionZ', 0, 500 )
.onChange( function () {
    if (objectCourant != null){
        objectCourant.position.z = param.positionZ;
    }

});

// lightFolder.add( param, 'prismeFace1_longueur', 0, 20 )
// .onChange( function () {
//     if (objectCourant != null  && objectCourant.geometry.type == "CylinderGeometry" ){
//         enleverDeLaScene(objectCourant.id);
//         var pyramidgeometry = new THREE.CylinderGeometry(objectCourant.geometry.parameters.radiusTop, 
//             param.prismeFace1_longueur, 
//             objectCourant.geometry.parameters.height, 
//             4, false);
//         var elementNouveau = new THREE.Mesh(pyramidgeometry, 
//             new THREE.MeshLambertMaterial({ color: Math.random() * 0xffffff, transparent : true }));
//         setPosition(elementNouveau, objectCourant.position.x, objectCourant.position.y);
//         ajouterALaScene(elementNouveau);
//            // objectCourant.geometry.parameters.radiusTop = param.prismeFace1_longueur;
//            // render();
//            // console.log(objectCourant.geometry.parameters.radiusTop);
//        }

//    });

// var folder = gui.addFolder( 'Morph Targets' );
// folder.add( param, 'influence1', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
//      if (objectCourant != null  && objectCourant.geometry.type == "CylinderGeometry" ){

//         objectCourant.morphTargetInfluences[ 0 ] = value;

//     }

    
// } );
// folder.add( param, 'influence2', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     meobjectCourantsh.morphTargetInfluences[ 1 ] = value;

// } );
// folder.add( param, 'influence3', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 2 ] = value;

// } );
// folder.add( param, 'influence4', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 3 ] = value;

// } );
// folder.add( param, 'influence5', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 4 ] = value;

// } );
// folder.add( param, 'influence6', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 5 ] = value;

// } );
// folder.add( param, 'influence7', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 6 ] = value;

// } );
// folder.add( param, 'influence8', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

//     objectCourant.morphTargetInfluences[ 7 ] = value;

// } );
lightFolder.add(obj,'duplicate');
lightFolder.add(obj,'delete');
lightFolder.add(obj,'fusion');
lightFolder.add(obj,'zoomAvant');
lightFolder.add(obj,'zoomArriere');
lightFolder.open();
gui.open();
}

buildGui();
