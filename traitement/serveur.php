<?php
header('Content-Type: application/json');
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
// if(isset($_POST["soumettreFichier"])) {
//     $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
//     if($check !== false) {
//         echo "File is an image - " . $check["mime"] . ".";
//         $uploadOk = 1;
//     } else {
//         echo "File is not an image.";
//         $uploadOk = 0;
//     }
// }
// Check if file already exists
$reponse['erreur'] = '';
$reponse['goodjson'] = '';
$reponse['goodimage'] = '';
// if (file_exists($target_file)) {
// 	$reponse['erreur'] .= 'Désolé, le fichier a été téléchargé et n\'a pas été supprimé<br>';
//     $uploadOk = 0;
// }
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
	$reponse['erreur'] .= 'Désolé, le fichier est trop lourd\n';
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "json" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif") {
	$reponse['erreur'] .= 'Désolé, seul les format json, png, jpeg et gif sont permis\n';
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	$reponse['erreur'] .= 'Désolé, le fichier n\'a pas été chargé.\n';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    	
        if($imageFileType == "json"){
            $reponse['goodjson'] .= "Le fichier ". basename( $_FILES["fileToUpload"]["name"]). " a été chargé.";
            $reponse['content'] = file_get_contents('uploads/'.basename( $_FILES["fileToUpload"]["name"]));

            unlink('uploads/'.basename( $_FILES["fileToUpload"]["name"]));
        }else{
            $reponse['goodimage'] .= "Le fichier ". basename( $_FILES["fileToUpload"]["name"]). " a été chargé.";
            $reponse['content'] = 'uploads/'.basename($_FILES["fileToUpload"]["name"]);

        }
        
    } else {
    	$reponse['erreur'] = "Désolé, il y a eu une erreur en chargant le fichier";
    }
}

echo json_encode($reponse);


?>

