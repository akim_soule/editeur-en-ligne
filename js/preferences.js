
function changerTheme() {
	var x = document.body;
	if (x.style.backgroundColor === "black") {
		x.style.backgroundColor = "white";
		scene.background = new THREE.Color("rgb(158, 158, 158)");
		document.getElementById('navigateur').style.backgroundColor = "white";
		let link = document.getElementsByTagName('A');
		for (var i = 0; i < link.length; i++) {
			link[i].style.color = "black";
		}
		let drop = document.getElementsByClassName("dropdown-menu");
		for (var i = 0; i < drop.length; i++) {
			drop[i].style.backgroundColor = "white";
		}
	} else {
		x.style.backgroundColor = "black";
		scene.background = new THREE.Color("rgb(243, 243, 243)");
		document.getElementById('navigateur').style.backgroundColor = "black";
		let link = document.getElementsByTagName('A');
		for (var i = 0; i < link.length; i++) {
			link[i].style.color = "white";
		}
		let drop = document.getElementsByClassName("dropdown-menu");
		for (var i = 0; i < drop.length; i++) {
			drop[i].style.backgroundColor = "black";
		}
	}
}

var jeton = 0;

function changerLogo() {
	if (jeton ==0){
		jeton = 1 ;
		document.getElementById('imgLogo').src = "./img/uchiha.png";
	}else if(jeton == 1){
		jeton = 0;
		document.getElementById('imgLogo').src = "./img/uzumaki.png";
	}
	
}


