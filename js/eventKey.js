function KeyPress(e) {
  console.log(e);
  var evtobj = window.event? event : e
  //control Z
  if (evtobj.keyCode == 90 && evtobj.ctrlKey){
    retour();
  }
  //control Y
  if (evtobj.keyCode == 89 && evtobj.ctrlKey) {
    refaire();
  }

  //couper
  if (evtobj.keyCode == 88 && evtobj.ctrlKey) {
    couper();
  }

  //copier
  if (evtobj.keyCode == 67 && evtobj.ctrlKey) {
    copier();
  }

  //coller
  if (evtobj.keyCode == 86 && evtobj.ctrlKey) {
    coller();

  }

  //reseter stage
  if (evtobj.keyCode == 82 && evtobj.ctrlKey) {
    reseterStage();
  }

  //supprimer
  if (evtobj.keyCode == 46) {
    supprimer();
  }

  //Tous selectionner
  if (evtobj.keyCode == 65 && evtobj.ctrlKey) {
    selectionnerTout();
  }

  //Tout deselectionner
  if (evtobj.keyCode == 65 && 
    evtobj.keyCode == 16 && 
    evtobj.ctrlKey) {
    deselectionner();
}

  //nouveau
  if (evtobj.keyCode == 78 && evtobj.ctrlKey) {
    nouveau();
  }

  //importer
  if (evtobj.keyCode == 80 && evtobj.keyCode != 16 && evtobj.ctrlKey) {
    importer();
  }

  //sauvegarder
  if (evtobj.keyCode == 83 && evtobj.ctrlKey) {
    sauvegarder(2);
  }

  //sauvegarder sous
  if (evtobj.keyCode == 68 && evtobj.ctrlKey) {
    sauvegarderSous();
  }

  //exporter objet selectionne
  if (evtobj.keyCode == 69 && evtobj.ctrlKey) {
    exporterObjectCourant();
  }

  //exporter tous les objets
  if (evtobj.keyCode == 70 && evtobj.ctrlKey) {
    exporterTous();
  }

  if (objectCourant != undefined) {
    if (evtobj.keyCode == 37) {
      objectCourant.position.x -= 1;
    }
    if (evtobj.keyCode == 38) {
      objectCourant.position.y += 1;
    }
    if (evtobj.keyCode == 39) {
      objectCourant.position.x += 1;
    }
    if (evtobj.keyCode == 40) {
      objectCourant.position.y -= 1;
    }
  }

  if(evtobj.keyCode == 17){
      cntrlIsPressed = true;
  }

}

function KeyUp(e) {

  var evtobj2 = window.event? event : e

  if(evtobj2.keyCode == 17){
      cntrlIsPressed = false;

  }
}


document.onkeydown = KeyPress;

document.onkeyup = KeyUp;




