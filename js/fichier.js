
function nouveau() {
  if (objects && objects.length > 0) {
    document.getElementById('modalSauvegarde').style.display='block';
  }
}

function importer() {
  document.getElementById('ouvirModal').style.display='block';
}

function sauvegarder(nombre) {
  if (objects && objects.length > 0) {
    switch(nombre){
      //format proprietaire
      case 0:
      sauvegarderAuFormatProprietaire();
      break;
      //format image
      case 1:
      sauvegarderAuFormatImage();
      break;
      //les deux format
      case 2:
      sauvegarderDansLesDeuxFormats();
      break;
    }
  }else{
    alert("Il y a rien à sauvegarder");
  }
}

function sauvegarderSous() {
  document.getElementById('sauvegarderSousModal').style.display='block';
}

function exporterObjectCourant() {
  if (objectCourant != undefined){
    var json = JSON.stringify(objectCourant);
    blob = new Blob([json], {type: "octet/stream"});
    url = window.URL.createObjectURL(blob);
    downloadURI(url, 'fichier.json');
  }else{
    if (objects && objects.length > 0) {
      alert("Veuillez sélectionner un objet");
    }else{
      alert("Veuillez créer au moins un objet et le sélectionner avant de l'exporter");
    }
    
  }
}

function exporterTous() {
  sauvegarderAuFormatProprietaire();
}

$(document).ready(function() {
  $('#sauvegarderOui').on('click', function(event) {
    sauvegarder(2);
  });
  $('#sauvegarderNon').on('click', function(event) {
    reseterStage();
  });

  $('#ouvrirFichier').on('click', function(event) {
    var file_data = $('#fileToUpload').prop('files')[0];
    if(file_data != undefined) {
      var form_data = new FormData();                  
      form_data.append('fileToUpload', file_data);
      $.ajax({
        url: 'traitement/serveur.php',
        contentType : false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        data: form_data
      })
      .done(function(reponse) {
        if (reponse.erreur != "") {
          alert(reponse.erreur);
        }
        if (reponse.goodimage != "") {

          alert(reponse.content);

          var loader = new THREE.TextureLoader();

          var material = new THREE.MeshLambertMaterial({
            map: loader.load('traitement/'+reponse.content)
          });

          var geometry = new THREE.PlaneGeometry(10, 10*.75);

          var mesh = new THREE.Mesh(geometry, material);

          mesh.position.set(0,0,0)

          addToScene(mesh);

        }
        if (reponse.goodjson != "") {
          alert(reponse.goodjson);
          deserialise(reponse.content);
        }
      })
      .fail(function(err) {
        console.log(err);
      })
      
      
      
    }else{
      alert("Veuillez selectionner un fichier");
      return false;
    }
    $('#fileToUpload').value = "";
    
  });

});


function reseterStage() {
 while(objects.length > 0){
  removeFromScene(objects.pop());
}
sleep(1000).then(() => {
  objects = [];
  actualiserControls(objects);
  renderer.render( scene, camera );
});

}

function deserialise(data){


  var myObj = JSON.parse(data);
  
  if (myObj.listObjetSerials != undefined) {
    console.log(myObj.listObjetSerials);
    while(myObj.listObjetSerials.length != undefined && 
      myObj.listObjetSerials.length > 0){
      let obji = myObj.listObjetSerials.pop();
    var i = obji.indiceForme;
    if (i != -1 && i >= 0 && i <= 12) {
      let element = FabriqueElement(i);
      console.log(obji.fill);
      element.material.color.set(obji.fill);
      addToScene(element);
    }

    if(i == -1 && 
      obji.listPathElementSerials != undefined){
      //case -1 
    var material = new THREE.MeshLambertMaterial( { color: obji.fill, transparent : true } );
    var shape = new THREE.Shape();
    var originX, originY;

    while(obji.listPathElementSerials.length > 0){

      let pathElement = obji.listPathElementSerials.shift();
      let nom = pathElement.nom;
      // shape.moveTo(0, 0);
      // shape.autoClose = true;
      switch (nom) {
        case "javafx.scene.shape.ArcTo":
        break;
        case "javafx.scene.shape.ClosePath":
        break;
        case "javafx.scene.shape.CubicCurveTo":
        shape.bezierCurveTo (
          pathElement.controlX1 - originX, 
          pathElement.controlY1 - originY, 
          pathElement.controlX2 - originX, 
          pathElement.controlY2 - originY,
          pathElement.x - originX, 
          pathElement.y - originY);
        break;
        case "javafx.scene.shape.HLineTo":
        break;
        case "javafx.scene.shape.LineTo":
        shape.lineTo(
          pathElement.x - originX , 
          pathElement.y - originY);
        break;
        case "javafx.scene.shape.QuadCurveTo":
        // shape.quadraticCurveTo(cpX: Float , cpY: Float , x: Float , y: Float );
        break;
        case "javafx.scene.shape.MoveTo":
        shape.moveTo(0, 0);
        originX = pathElement.x;
        originY = pathElement.y;
        // console.log('originX : '+originX);
        // console.log('originY : '+originY);
        break;
        case "javafx.scene.shape.VLineTo":
        break;
        default:
        break;
      }

    }
    var geometryShape = new THREE.ShapeGeometry(shape);
    var element = new THREE.Mesh(geometryShape, material);
    element.scale.set(0.1,0.1,0.1);
    element.rotateZ( Math.PI);
    // element.rotateY( Math.PI);
    // setPosition(element, 0, 0);
    addToScene(element);
    console.log(i);
  }
}



}else{
  if (myObj.length == undefined) {
    let loader = new THREE.ObjectLoader();
    let object = loader.parse(myObj);
    addToScene(object);
  }
  for (var i = 0; i < myObj.length; i++) {
    if (myObj[i].geometries.length > 0) {
      let loader = new THREE.ObjectLoader();
      let object = loader.parse(myObj[i]);
      addToScene(object);
    }
  }

}

renderer.render( scene, camera );

}


function sauvegarderAuFormatProprietaire() {
  if (objects.length > 0) {
    var json = JSON.stringify(objects);
    blob = new Blob([json], {type: "octet/stream"});
    url = window.URL.createObjectURL(blob);
    downloadURI(url, 'fichier.json');
  }else{
    alert("Il y a rien à sauvegarder");
  }
}

function sauvegarderDansLesDeuxFormats() {
  sauvegarderAuFormatProprietaire();
  sauvegarderAuFormatImage();
}

function sauvegarderAuFormatImage(){
  if (objects.length > 0) {
    var img = new Image();
    renderer.render(scene, camera);
    img.src = renderer.domElement.toDataURL();
    downloadURI(img.src, 'stage.png');
  }else{
    alert("Il y a rien à sauvegarder");
  }
}

function downloadURI(uri, name) {
  var link = document.createElement('a');
  link.download = name;
  link.href = uri;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}

function returnGeometry(argument) {
  let type = argument.type;
  var geometry = null;
  switch(type){
    case 'BoxGeometry':
    let height = argument.height;
    let width = argument.width;
    let depth = argument.depth;
    geometry = new THREE.BoxGeometry(width, height, depth);
    break;
    default:
    geometry = new THREE.BoxGeometry(width, height, depth);
    break;
  }
  return geometry;
}



const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

