

var undoCommand = [];
var redoCommand = [];

function pushCommandeInUndo(action, node) {
  undoCommand.push(new Couple(action, node));
}

function popCommande(){
  var couple = null;
  if (undoCommand !== undefined &&  
    undoCommand.length != 0) {
    couple = undoCommand[undoCommand.length - 1];
  redoCommand.push(undoCommand.pop());
}
return couple;
}

function popCommandeFromRedo(){
  var couple = null;
  if (redoCommand !== undefined &&
    redoCommand.length != 0) {
    couple = redoCommand[redoCommand.length - 1];
  undoCommand.push(redoCommand.pop());
}
return couple;
}

var ACTION = {
  AJOUTER :"AJOUTER",
  SUPPRIMER :"SUPPRIMER"
}

class Commande{
  constructor(action, node){
    switch(action){
      case ACTION.AJOUTER :
      pushCommandeInUndo(ACTION.SUPPRIMER, node);
      break;
      case ACTION.SUPPRIMER :
      pushCommandeInUndo(ACTION.AJOUTER, node);
      break;
    }
  }
}

class Couple{
  constructor(action, node){
    this.action = action;
    this.node = node;
  }

  getAction(){
    return this.action;
  }
  setAction(action){
    this.action = action;
  }
  getNode(){
    return this.action;
  }
  setNode(node){
    this.node = node;
  }
}
