function retour() {
  let couple = popCommande();
  if (couple != null) {
    switch(couple.action){
      case 'SUPPRIMER':
      enleverDeLaScene(couple.node.id);
      break;
      case 'AJOUTER':
      ajouterALaScene(couple.node);
      break;
    }
  }
}

function refaire() {
  let couple = popCommandeFromRedo();
  if (couple != null) {
    switch(couple.action){
      case 'SUPPRIMER':
      ajouterALaScene(couple.node);
      break;
      case 'AJOUTER':
      enleverDeLaScene(couple.node.id);
      break;
    }
  }
}


function couper() {
  if (objectCourant !== undefined) {
    objetAColler = objectCourant;
    removeFromScene(objectCourant);
  }

}

function copier() {
  if (objectCourant !== undefined) {
    objetAColler = objectCourant;
  }

}

function coller() {
 if (objetAColler !== undefined) {
  var objetClone = objetAColler.clone();
  if (objectCourant.material.map != null) {
    console.log(objectCourant.material.map.image.attributes[0]);
    var loader = new THREE.TextureLoader();
    var img = new String(objectCourant.material.map.image.attributes[0]);
    var mat = new THREE.MeshLambertMaterial({
      map: loader.load(img),
      transparent : true
    });
  }else{
    var mat = new THREE.MeshLambertMaterial( { color: objectCourant.material.color, transparent : true } );
    objetClone.material = mat;
  }

  for (var i = 0; i < objects.length; i++) {
    if (objects[i].position.x == objetClone.position.x || objects[i].position.y == objetClone.position.y) {
      setPosition(objetClone, objects[i].position.x +2, 
        objects[i].position.y - 2);
    }
  }
  addToScene(objetClone);
}
}

function supprimer() {
  if (objectCourant !== undefined) {
    removeFromScene(objectCourant);
  }
}


function ajouterALaScene(element) {
  scene.add(element);
  objects.push(element);
}

function enleverDeLaScene(id) {
  var object = scene.getObjectById( id, true );
  scene.remove(object);
  for( var i = 0; i < objects.length; i++){ 
   if ( objects[i].id === id) {
     objects.splice(i, 1); 
   }
 }
}